use std::io;

#[derive(Debug, PartialEq)]
struct Deplacement{
    x :i32,
    y :i32
}

#[derive(Debug, PartialEq)]
enum Orientation {
    Nord,
    Sud,
    Est,
    Ouest,
}

#[derive(Debug, PartialEq)]
enum Instruction {
    L,
    R,
    F,
}

/// li la valeur et fait un match pour voir avec quel instruction elle correspond en "string"
fn read_char(valeur: &str) -> Result<Instruction, ()> {
    match valeur {
        "L" => Ok(Instruction::L),
        "R" => Ok(Instruction::R),
        "F" => Ok(Instruction::F),
        _ => Err(()),
    }
}

/// li la valeur et fait un match pour voir avec quel orientation elle correspond en "string" et si erreur de saisie Nord part défaut
fn read_orientation(valeur: &str) -> Result<Orientation, ()> {
    use Orientation as O;
    match valeur {
        "N" => Ok(O::Nord),
        "W" => Ok(O::Ouest),
        "S" => Ok(O::Sud),
        "E" => Ok(O::Est),
        _ =>  Err(()),
    }
}

fn main() {
    let o = read_orientation("S").expect("This is not fine");
    let d = read_char("F").expect("Didn't expected that!");
    println!(" ori =  {:#?}  dire = {:#?} ",o,d);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_Orientation_south() {
        assert_eq!(read_orientation("S"), Ok(Orientation::Sud));
    }
    #[test]
    fn test_Orientation_north() {
        assert_eq!(read_orientation("N"), Ok(Orientation::Nord));
    }
    #[test]
    fn test_Orientation_erreur() {
        assert_eq!(read_orientation(""), Ok(Orientation::Sud));
    }
}